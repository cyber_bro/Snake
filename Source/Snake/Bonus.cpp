// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"

#include "SnakeBase.h"

// Sets default values
ABonus::ABonus()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABonus::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABonus::Interact(AActor* Interactor, const bool bIsHead)
{
	if (bIsHead)
	{
		const auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(Snake->MovementSpeed / 2);
			Snake->AccelerationTicksLeft = 20;
			this->Destroy();
		}
	}
}
